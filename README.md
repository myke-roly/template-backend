# API App

## Development

- `yarn`
- `yarn start`
- API running on defined `PORT` in `.env` or port 8000.

(Remember to run `nvm use` in the root).

- ### `yarn start`

  Runs the app in dev mode on defined `PORT` in `.env` or port 8000.

- ### `yarn lint`

  Runs Eslint validations.

- ### `yarn lint:fix`

  Runs Eslint validation and tries to fix current issues if possible.


## Branches

- Master: Production branch
- Develop: Develop branch used by devs to work on new features. Always create a new feature branch from here.

### Naming convention

Always use `kebab-case` when naming branches.

- Feature branches: `{project-shorthand}-{ticket-number}/{type}/{description}`.
  Example: `MAS-56/fix/drawer-wont-open`
- Hotfix branches (branches without ticket that need to be worked on inmediately): `hotfix/my-description`
  Examples: `hotfix/user-not-able-to-login`

**Possible types:**

- `feat`: A new feature
- `fix`: A bug fix
- `docs`: Documentation only changes
- `style`: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- `refactor`: A code change that neither fixes a bug nor adds a feature
- `perf`: A code change that improves performance
- `test`: Adding missing or correcting existing tests
- `chore`: Changes to the build process or auxiliary tools and libraries such as documentation generation

## Merge Requests

Use commitizen to format commits in order to have a unified commit template to fill in.

- `npm i -g commitizen`

- Use `git cz` instead of `git commit`

- Follow the walkthrough and add the relevant information always including the ticket number with it's project shorthand for proper linking to Jira ticket, e.g. `MAS-56`

#### Push branch

- On feature branch
- `git pull origin development:development`
- if conflicts, solve them
- `git push origin ...`
