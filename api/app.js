import express from 'express'
import cors from 'cors'
import mongoSanitize from 'express-mongo-sanitize'
import compression from 'compression'
import helmet from 'helmet'
import xss from 'xss-clean'

import config from './config'
import { errorHandler, requestLogger } from './middlewares'
import './middlewares/authenticate'
import { authLimiter } from './middlewares/rateLimiter'
import { mainRouter } from './routes'
import { db } from './utils/dbConection'
import { logger } from './utils/logger'

const { port } = config
const app = express()

// set security HTTP headers
app.use(helmet())

// parse json request body and parse urlencoded request body
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ extended: true, limit: '50mb' }))

// sanitize data
app.use(xss())
app.use(mongoSanitize())

// gzip compression
app.use(compression())

// enable cors
app.use(cors())
app.options('*', cors())

// logger middleware
app.use(requestLogger)

// limit repeated failed requests to auth endpoints
if (config.env === 'production') {
  app.use('/v1/auth', authLimiter)
}

app.use(mainRouter)
app.use(errorHandler)

let server
db.then(() => {
  logger.info('Connected to MongoDB')
  server = app.listen(port, () => {
    logger.info(`Server listening on port ${port}`)
  })
})

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.warning('Server closed')
      process.exit(1)
    })
  } else {
    process.exit(1)
  }
}

const unexpectedErrorHandler = error => {
  logger.error(error)
  exitHandler()
}

process.on('uncaughtException', unexpectedErrorHandler)
process.on('unhandledRejection', unexpectedErrorHandler)

process.on('SIGTERM', () => {
  logger.info('SIGTERM received')
  if (server) {
    server.close()
  }
})
