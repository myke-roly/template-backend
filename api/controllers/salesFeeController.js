import { celebrate, Joi, Segments } from 'celebrate'

import SalesFee from '../models/SalesFee'

export const validateSalesFee = celebrate({
  [Segments.BODY]: Joi.object().keys({
    currentPercentage: Joi.number().required(),
    period: Joi.string().optional().allow(''),
    description: Joi.string().optional().allow(''),
  }),
})

export const createSalesFee = (req, res, next) => {
  const salesFee = new SalesFee(req.body)
  salesFee.isDeleted = false
  salesFee.save(function (err) {
    if (err) return next(err)
    res.send(salesFee)
  })
}
export const getSalesFee = (req, res, next) => {
  SalesFee.find({ isDeleted: false }, function (err, salesFee) {
    if (err) {
      return next(err)
    }
    return res.send(salesFee)
  })
}

export const updateSalesFee = (req, res, next) => {
  SalesFee.findByIdAndUpdate(req.params.id, req.body, { new: true }, (err, salesFee) => {
    if (err) {
      return next(err)
    }
    return res.send(salesFee)
  })
}

export const deleteSalesFee = (req, res, next) => {
  SalesFee.findByIdAndUpdate(req.params.id, { isDeleted: true }, { new: true }, (err, salesFee) => {
    if (err) {
      return next(err)
    }
    return res.send(salesFee)
  })
}
