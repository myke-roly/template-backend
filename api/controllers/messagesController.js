import { celebrate, Joi, Segments } from 'celebrate'

import Messages from '../models/Messages'

export const validateMessage = celebrate({
  [Segments.BODY]: Joi.object().keys({
    userId: Joi.string().required(),
    message: Joi.string().required(),
  }),
})

export const createMessage = (req, res, next) => {
  const { user } = req
  const data = { ...req.body, ownerId: user._id }
  const message = new Messages(data)
  message.save(function (err) {
    if (err) {
      return next(err)
    }
    res.send(message)
  })
}

export const getMessages = async (req, res, next) => {
  const { user } = req
  // const { limit, sort, page, by, ...rest } = req.query
  // const totalMessages = await Messages.countDocuments({ ...rest, isDeleted: false })
  // const sortValue = !!(by && sort) ? { [by]: sort } : null

  Messages.find({ ownerId: user._id })
    .populate('userId')
    .then(messages => {
      const threadsMap = {}
      messages.forEach(m => {
        const { userId, message, createdAt } = m
        threadsMap[userId._id] = threadsMap[userId._id] || []
        threadsMap[userId._id].push({ user: userId, message, createdAt })
      })
      res.send({ messages: Object.values(threadsMap) })
    })
    .catch(err => next(err))
}
