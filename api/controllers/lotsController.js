import { celebrate, Joi, Segments } from 'celebrate'

import Lot from '../models/Lots'
import Parking from '../models/Parkings'

export const validateLot = celebrate({
  [Segments.BODY]: Joi.object().keys({
    parkingId: Joi.string(),
    lotRef: Joi.string(),
    image: Joi.string(), // Luego validar required
    description: Joi.string().required(),
    features: Joi.object().keys({
      security: Joi.boolean(),
      underCover: Joi.boolean(),
    }),
    prices: Joi.object().keys({
      hr: Joi.number(),
      week: Joi.number(),
      month: Joi.number(),
    }),
    rating: Joi.number().min(0).max(10),
    dimensions: Joi.object().keys({
      width: Joi.number(),
      height: Joi.number(),
      length: Joi.number(),
    }),
  }),
})

export const createLot = (req, res, next) => {
  const lot = new Lot(req.body)
  lot.save(function (err) {
    if (err) return next(err)
    Parking.findByIdAndUpdate(
      lot.parkingId,
      { $push: { lots: lot._id } },
      { new: true },
      (errParking, parking) => {
        if (errParking) return next(errParking)
        res.send({ lot, parking })
      },
    )
  })
}

export const getLots = async (req, res, next) => {
  const { limit, sort, page, by, ...rest } = req.query
  const totalLots = await Lot.countDocuments({ ...rest, isDeleted: false })
  const sortValue = !!(by && sort) ? { [by]: sort } : null

  Lot.find({ ...rest, isDeleted: false })
    .sort(sortValue)
    .limit(limit * 1)
    .skip((page - 1) * limit)
    .then(lots => {
      const totalPages = Math.ceil(totalLots / limit)
      const currentPage = page
      if (totalPages && currentPage) res.send({ lots, totalPages, currentPage })
      else res.send({ lots })
    })
    .catch(err => next(err))
}

export const updateLot = (req, res, next) => {
  Lot.findByIdAndUpdate(req.params.id, req.body, { new: true }, (err, lot) => {
    if (err) {
      return next(err)
    }
    return res.send(lot)
  })
}

export const deleteLot = (req, res, next) => {
  Lot.findByIdAndUpdate(req.params.id, { isDeleted: true }, { new: true }, (err, lot) => {
    if (err) {
      return next(err)
    }
    return res.send(lot)
  })
}

export const findLotsByParkingId = (req, res, next) => {
  const { id } = req.params
  Lot.find({ parkingId: id }, { isDeleted: false }, (err, lot) => {
    if (err) {
      return next(err)
    }
    return res.send(lot)
  })
}
