import { celebrate, Joi, Segments } from 'celebrate'
import jwt from 'jsonwebtoken'
import passport from 'passport'

import { errorCode, parkingCode, UserErrorCode } from 'api/constants/error-code'

import config from '../config'
import Users from '../models/Users'
import Parkings from '../models/Parkings'

const { jwtSecret, jwtExpiration } = config

export const validateUsers = celebrate({
  [Segments.BODY]: Joi.object().keys({
    name: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string()
      .pattern(new RegExp(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/))
      .required(),
    cellPhone: Joi.string().required(),
    image: Joi.optional(),
    birthday: Joi.string().required(),
  }),
})

export const validateUpdateUsers = celebrate({
  [Segments.BODY]: Joi.object().keys({
    name: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    cellPhone: Joi.string().required(),
    birthday: Joi.string().required(),
    image: Joi.optional(),
  }),
})

export const createUser = async (req, res) => {
  const { email } = req.body

  try {
    let user = await Users.findOne({ email })
    if (!!user) {
      return res.status(400).send({ code: UserErrorCode.USER_ALREDY_EXIST })
    }

    user = new Users({ ...req.body })
    const newUser = await user.save()
    const { password, ...userCreated } = newUser._doc
    return res.status(201).send(userCreated)
  } catch (error) {
    return res.status(500).send({ code: errorCode.UNKNOW_ERROR })
  }
}

const authenticatedUser = (strategyName, req, res, next) => {
  passport.authenticate(strategyName, async (err, user, info) => {
    try {
      if (err || !user) {
        const error = info?.message ? new Error(info.message) : new Error(err)
        return next(error)
      }

      req.login(user, { session: false }, error => {
        if (error) return next(error)

        const bodyJWT = { _id: user._id, email: user.email }
        const token = jwt.sign({ user: bodyJWT }, jwtSecret, { expiresIn: jwtExpiration })

        return res.status(201).json({ user, token })
      })
    } catch (error) {
      return next(error)
    }
  })(req, res, next)
}

export const loginUser = (req, res, next) => {
  authenticatedUser('login', req, res, next)
}

export const loginUserBySocialMedia = (req, res, next) => {
  authenticatedUser('loginSocialMedia', req, res, next)
}

export const findUserById = (req, res, next) => {
  const { id } = req.params
  Users.findById(id)
    .then(user => res.status(201).send(user))
    .catch(err => {
      return next(err)
    })
}

export const updateUserById = (req, res, next) => {
  const { id } = req.params
  const update = req.body
  Users.findByIdAndUpdate(id, update, { new: true }, (err, user) => {
    if (err) {
      return next(err)
    }
    res.status(201).send(user)
  })
}

export const deleteUserById = (req, res, next) => {
  const { id } = req.params
  const update = { isDeleted: true }
  Users.findByIdAndUpdate(id, update, { new: true }, (err, user) => {
    if (err) {
      return next(err)
    }
    res.status(201).send(user)
  })
}

export const findParkingsByUserId = async (req, res) => {
  const { id } = req.params

  try {
    const parkings = await Parkings.find({ createdBy: id })

    if (!parkings.length) {
      return res.status(404).send({ code: parkingCode.NOT_FOUND, parkings })
    }

    res.status(200).send({ parkings })
  } catch (error) {
    return res.status(500).send({ code: errorCode.UNKNOW_ERROR })
  }
}
