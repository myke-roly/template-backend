import { celebrate, Joi, Segments } from 'celebrate'

import Vehicles from '../models/Vehicles'
import Users from '../models/Users'

export const validateVehicle = celebrate({
  [Segments.BODY]: Joi.object().keys({
    brand: Joi.string().required(),
    model: Joi.string().required(),
    licensePlate: Joi.string().required(),
    color: Joi.string().required(),
    type: Joi.string().required().valid('car', 'motorcicle', 'suv', 'quad'),
    dimensions: Joi.object().keys({
      width: Joi.number(),
      height: Joi.number(),
      length: Joi.number(),
    }),
  }),
})

export const createVehicle = (req, res, next) => {
  const { user } = req
  const vehicle = new Vehicles(req.body)
  vehicle.userId = user._id
  vehicle.save(function (err) {
    if (err) return next(err)
    Users.findByIdAndUpdate(
      user._id,
      { $push: { vehicles: vehicle._id } },
      { new: true },
      (error, user) => {
        if (error) return next(error)
        res.send(vehicle)
      },
    )
  })
}

export const getVehicles = async (req, res, next) => {
  const { limit, sort, page, by, ...rest } = req.query
  const totalVehicles = await Vehicles.countDocuments({ ...rest, isDeleted: false })
  const sortValue = !!(by && sort) ? { [by]: sort } : null

  Vehicles.find({ ...rest, isDeleted: false })
    .sort(sortValue)
    .limit(limit * 1)
    .skip((page - 1) * limit)
    .then(vehicles => {
      const totalPages = Math.ceil(totalVehicles / limit)
      const currentPage = page
      if (totalPages && currentPage) res.send({ vehicles, totalPages, currentPage })
      else res.send({ vehicles })
    })
    .catch(err => next(err))
}

export const findUserVehicles = (req, res, next) => {
  const { user } = req
  Vehicles.find({ userId: user._id }, function (err, vehicles) {
    if (err) {
      return next(err)
    }
    return res.send(vehicles)
  })
}

export const updateVehicle = (req, res, next) => {
  Vehicles.findByIdAndUpdate(req.params.id, req.body, { new: true }, (err, lot) => {
    if (err) {
      return next(err)
    }
    return res.send(lot)
  })
}

export const deleteVehicle = (req, res, next) => {
  const { id } = req.params
  Vehicles.findByIdAndDelete(id)
    .then(() => res.send({ isDeleted: 'ok' }))
    .catch(err => next(err))
}
