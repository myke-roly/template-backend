import { celebrate, Joi, Segments } from 'celebrate'

import Booking from '../models/Bookings'

export const validateBooking = celebrate({
  [Segments.BODY]: Joi.object().keys({
    checkin: Joi.date().required(),
    checkout: Joi.date()
      /*  .ruleset.greater(Joi.ref('checkin'))
      .rule({ message: 'checkout must be greater than checkin' }) */
      .required(),
    time: Joi.date().required(),
    price: Joi.number().required(),
    locationGPS: Joi.object().keys({
      type: Joi.string().valid('Point'),
      coordinates: Joi.array().items(Joi.number(), Joi.number()).min(2).max(2),
    }),
  }),
})

export const createBooking = (req, res, next) => {
  const { user } = req
  const booking = new Booking(req.body)
  booking.userId = user._id
  booking.save(function (err) {
    if (err) return next(err)
    res.send(booking)
  })
}

export const getBooking = async (req, res, next) => {
  const { limit, sort, page, by, ...rest } = req.query
  const totalBookings = await Booking.countDocuments({ ...rest, isDeleted: false })
  const sortValue = !!(by && sort) ? { [by]: sort } : null

  Booking.find({ ...rest, isDeleted: false })
    .sort(sortValue)
    .limit(limit * 1)
    .skip((page - 1) * limit)
    .then(bookings => {
      const totalPages = Math.ceil(totalBookings / limit)
      const currentPage = page
      if (totalPages && currentPage) res.send({ bookings, totalPages, currentPage })
      else res.send({ bookings })
    })
    .catch(err => next(err))
}

export const updateBooking = (req, res, next) => {
  Booking.findOneAndUpdate(req.params.id, req.body, { new: true }, (err, booking) => {
    if (err) {
      return next(err)
    }
    return res.send(booking)
  })
}
