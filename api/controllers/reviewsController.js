import { celebrate, Joi, Segments } from 'celebrate'

import Reviews from '../models/Reviews'

export const validateReview = celebrate({
  [Segments.BODY]: Joi.object().keys({
    comment: Joi.string().required(),
    score: Joi.number().required(),
    isChecked: Joi.boolean(),
  }),
})

export const createReview = (req, res, next) => {
  const { user } = req
  const { lotId } = req.params
  const review = new Reviews(req.body)
  review.userId = user._id
  review.lotId = lotId
  review.save(function (err) {
    if (err) {
      return next(err)
    }
    res.send(review)
  })
}

export const getReviews = async (req, res, next) => {
  const { limit, sort, page, by, ...rest } = req.query
  const totalReviews = await Reviews.countDocuments({ ...rest, isDeleted: false })
  const sortValue = !!(by && sort) ? { [by]: sort } : null

  Reviews.find({ ...rest, isDeleted: false })
    .sort(sortValue)
    .limit(limit * 1)
    .skip((page - 1) * limit)
    .then(reviews => {
      const totalPages = Math.ceil(totalReviews / limit)
      const currentPage = page
      if (totalPages && currentPage) res.send({ reviews, totalPages, currentPage })
      else res.send({ reviews })
    })
    .catch(err => next(err))
}

export const getLotReviews = (req, res, next) => {
  const { lotId } = req.params
  Reviews.find({ lotId }, function (err, reviews) {
    if (err) {
      return next(err)
    }
    return res.send(reviews)
  })
}

export const updateReview = (req, res, next) => {
  const { id } = req.params
  const update = req.body
  Reviews.findByIdAndUpdate(id, update, { new: true }, (err, reviews) => {
    if (err) {
      return next(err)
    }
    res.status(201).send(reviews)
  })
}

export const deleteReview = (req, res) => {
  const { id } = req.params
  Reviews.findByIdAndDelete(id)
    .then(() => res.send({ isDeleted: 'ok' }))
    .catch(err => res.send(err))
}
