import { celebrate, Joi, Segments } from 'celebrate'

import Parking from '../models/Parkings'

export const validateParking = celebrate({
  [Segments.BODY]: Joi.object().keys({
    status: Joi.string().required(),
    createdBy: Joi.string().required(),
  }),
})

export const createParking = async (req, res, next) => {
  const { info } = req.body
  try {
    let parking = await Parking.findOne({ 'info.name': info?.name || '' })
    if (!!parking) return res.status(400).send({ message: 'Ya existe un parking con ese nombre' })

    parking = new Parking(req.body)
    await parking.save()
    return res.status(201).send({ parking })
  } catch (error) {
    next(error)
    return res.status(500).send({ message: 'Error del servidor' })
  }
}

export const getParkings = async (req, res, next) => {
  const { limit, sort, page, by, ...rest } = req.query
  const totalParkings = await Parking.countDocuments({ ...rest, isDeleted: false })
  const sortValue = !!(by && sort) ? { [by]: sort } : null

  Parking.find({ ...rest })
    .sort(sortValue)
    .limit(limit * 1)
    .skip((page - 1) * limit)
    .then(parkings => {
      const totalPages = Math.ceil(totalParkings / limit)
      const currentPage = page
      if (totalPages && currentPage) res.send({ parkings, totalPages, currentPage })
      else res.send({ parkings })
    })
    .catch(err => next(err))
}

export const updateParking = async (req, res) => {
  const { id: _id } = req.params
  try {
    const parkingUpdate = await Parking.findByIdAndUpdate({ _id }, req.body, { new: true })
    if (!parkingUpdate) return res.status(404).send({ message: 'No se encontro parking' })

    return res.status(201).send({ parkingUpdate })
  } catch (error) {
    return res.status(500).send({ message: 'Error del servidor ' })
  }
}

export const deleteParking = async (req, res) => {
  const { id: _id } = req.params
  try {
    const parkingDeleted = await Parking.findOneAndDelete({ _id })
    if (!parkingDeleted) return res.status(404).send({ message: 'No se encontro parking' })

    return res.status(200).send({ message: 'Parking eliminado' })
  } catch (error) {
    return res.status(500).send({ message: 'Error de servidor' })
  }
}

export const deleteParkings = async (req, res) => {
  const { id } = req.params
  try {
    const parkingDeleted = await Parking.remove({ createdBy: id })
    if (!parkingDeleted) return res.status(404).send({ message: 'No se encontro parkings' })

    return res.status(200).send({ message: 'Parkings eliminado', parkingDeleted })
  } catch (error) {
    return res.status(500).send({ message: 'Error de servidor' })
  }
}

export const getParking = async (req, res, next) => {
  const { id: _id } = req.params
  try {
    const parking = await Parking.findOne({ _id })
    if (!parking) return res.status(404).send({ message: 'No se encontro parking' })

    return res.status(200).send({ parking })
  } catch (error) {
    res.status(500).send({ message: 'Error en el servidor' })
    return next(error)
  }
}

export const getClosestParking = (req, res, next) => {
  const { lng, lat, maxDistance } = req.query
  Parking.find({
    isDeleted: false,
    location: {
      $near: {
        $maxDistance: maxDistance,
        $geometry: {
          type: 'Point',
          coordinates: { lat, lng },
        },
      },
    },
  })
    .populate('userId')
    .exec((err, parkings) => {
      if (err) {
        return next(err)
      }
      return res.send(parkings)
    })
}
