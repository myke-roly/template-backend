/* eslint-disable no-unused-vars */
import { isCelebrate } from 'celebrate'

import { handleErrors } from 'api/constants/error-code'
import { GenericError, ValidationError } from 'api/errors'

const getResponseError = error => {
  let code = 500
  if (isCelebrate(error)) return new ValidationError(error)
  let commonError = error
  if (commonError instanceof GenericError) return commonError

  // MongoError handler
  if (error.code && error.code === 11000) {
    const field = Object.keys(error.keyValue)
    commonError = { ...commonError, message: handleErrors[field] }
    code = 409
  }

  return new GenericError(code, commonError.message)
}

const errorHandler = (error, req, res, next) => {
  const responseError = getResponseError(error)
  return res
    .status(responseError.status)
    .send({ error: { ...responseError, message: responseError.message } })
}

export { errorHandler }
