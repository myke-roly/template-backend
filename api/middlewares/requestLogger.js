import { Logger } from 'api/features/logger'

const requestLogger = (req, res, next) => {
  res.on('finish', () => {
    Logger.info(
      `${req.method.toUpperCase()} ${req.originalUrl} ${res.statusCode} BODY: ${JSON.stringify(
        req.body,
      )}`,
    )
  })
  next()
}

export { requestLogger }
