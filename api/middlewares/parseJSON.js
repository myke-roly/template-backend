export const parseJSON = key => (req, res, next) => {
  // eslint-disable-next-line no-param-reassign
  req.body[key] = JSON.parse(req.body[key])
  next()
}
