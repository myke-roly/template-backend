import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { Strategy as JWTStrategy, ExtractJwt as ExtractJWT } from 'passport-jwt'

// CONFIG
import config from '../config'
// DB USERS
import User from '../models/Users'

const { jwtSecret } = config

// REGISTRATION STRATEGY
const signUpStrategy = new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true,
  },
  async (req, email, password, done) => {
    try {
      const user = await User.create(req.body)
      return done(null, user)
    } catch (error) {
      return done(error)
    }
  },
)

passport.use('signup', signUpStrategy)

//  STRATEGY FOR LOGIN
const signInStrategy = new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password',
  },
  async (email, password, done) => {
    try {
      const user = await User.findOne({ email })
      if (!user)
        return done(null, false, {
          message: 'El correo electrónico o la contraseña no son correctos',
        })

      const validate = await user.isValidPassword(password)
      if (!validate) {
        return done(null, false, {
          message: 'El correo electrónico o la contraseña no son correctos',
        })
      }

      return done(null, user, { message: 'Login successfull' })
    } catch (error) {
      return done(error)
    }
  },
)

passport.use('login', signInStrategy)

const signInSocialMediaStrategy = new LocalStrategy(
  { usernameField: 'email' },
  async (email, _, done) => {
    try {
      const user = await User.findOne({ email })

      if (!user) done(null, false, { message: 'CLIENT_NOT_EXIST' })

      return done(null, user, { message: 'SUCCESS_LOGIN' })
    } catch (error) {
      return done(error)
    }
  },
)

passport.use('loginSocialMedia', signInSocialMediaStrategy)

// VERIFY THE JWT
passport.use(
  new JWTStrategy(
    {
      secretOrKey: jwtSecret,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        return done(null, token.user)
      } catch (e) {
        return done(e)
      }
    },
  ),
)
