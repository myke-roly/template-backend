import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

import config from 'api/config'

const { jwtSecret } = config

const generateJwt = (payload, type, options) => {
  return jwt.sign({ ...payload, jwtType: type }, jwtSecret, options)
}

const hashPassword = async password => {
  return bcrypt.hash(password, 10)
}

const comparePassword = async (password, hash) => {
  return bcrypt.compare(password, hash)
}

export const hashUtils = { generateJwt, hashPassword, comparePassword }
