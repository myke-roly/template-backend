import mongoose from 'mongoose'

import config from '../config'
import { logger } from './logger'

const { dbUser, dbPass, dbHost, dbName } = config
const uri = `mongodb+srv://${dbUser}:${dbPass}@${dbHost}/${dbName}?retryWrites=true&w=majority`

const db = mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
})

mongoose.set('useFindAndModify', false)

mongoose.connection.on('error', error => {
  logger.error(error.message)
})

mongoose.connection.on('disconnected', () => {
  logger.info('Mongoose connection is disconnected')
})

process.on('SIGINT', async () => {
  await mongoose.connection.close()
  process.exit(0)
})

export { db }
