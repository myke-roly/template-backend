export const UserErrorCode = Object.freeze({
  USER_ALREDY_EXIST: 'USER_ALREDY_EXIST',
  USER_NOT_EXIST: 'USER_NOT_EXIST',
})

export const parkingCode = Object.freeze({
  NOT_FOUND: 'NOT_FOUND',
  CREATED: 'CREATED',
})

export const errorCode = Object.freeze({
  UNKNOW_ERROR: 'UNKNOW_ERROR',
})

export const handleErrors = {
  email: UserErrorCode.USER_ALREDY_EXIST,
}
