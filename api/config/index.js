export default {
  port: process.env.PORT ?? 8100,
  env: process.env.NODE_ENV ?? 'development',
  jwtSecret: process.env.JWT_SECRET ?? 'secret',
  jwtExpiration: process.env.JWT_EXPIRATION ?? '2 days',
  dbUser: process.env.DB_USER,
  dbPass: process.env.DB_PASS,
  dbName: process.env.DB_NAME,
  dbHost: process.env.DB_HOST,
}
