import mongoose from 'mongoose'

const { Schema } = mongoose

const bookingSchema = new Schema(
  {
    ownerId: {
      type: Schema.Types.ObjectId,
      ref: 'Users',
      required: true,
    },
    clientId: {
      type: Schema.Types.ObjectId,
      ref: 'Users',
      required: true,
    },
    parkingId: {
      type: Schema.Types.ObjectId,
      red: 'Parkings',
    },
    checkin: {
      type: Date,
      required: true,
    },
    checkout: {
      type: Date,
      required: true,
    },
    time: {
      type: Date,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    status: {
      type: String,
    },
    payment: {
      type: Schema.Types.Mixed,
    },
    locationGPS: {
      type: { type: String, enum: ['Point'], default: 'Point' },
      coordinates: [Number],
    },
  },
  {
    timestamps: true,
  },
)

bookingSchema.index({ locationGPS: '2dsphere' })

export default mongoose.model('Bookings', bookingSchema)
