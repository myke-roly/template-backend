import mongoose from 'mongoose'

const { Schema } = mongoose

const salesFeeSchema = new Schema(
  {
    currentPercentage: {
      type: Number,
      required: true,
    },
    period: String,
    description: String,
    isDeleted: Boolean,
  },
  {
    timestamps: true,
  },
)

export default mongoose.model('SalesFee', salesFeeSchema)
