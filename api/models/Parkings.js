import mongoose from 'mongoose'

const { Schema } = mongoose

// const parkingSchema = new Schema(
//   {
//     userId: {
//       type: Schema.Types.ObjectId,
//       ref: 'Users',
//       required: true,
//     },
//     name: {
//       type: String,
//       required: true,
//       unique: true,
//     },
//     description: {
//       type: String,
//     },
//     accessInstruction: {
//       type: String,
//     },
//     termsAndConditions: {
//       type: String,
//     },
//     salesFeeId: {
//       type: Schema.Types.ObjectId,
//       ref: 'SalesFee',
//     },
//     height: {
//       type: String,
//     },
//     width: {
//       type: String,
//     },
//     typeOfAccess: {
//       type: String,
//     },
//     haveRamp: {
//       type: String,
//     },
//     isRoofing: {
//       type: String,
//     },
//     accesibility: [],
//     hour: {
//       type: Boolean,
//       default: false,
//     },
//     halfStay: {
//       type: Boolean,
//       deafault: false,
//     },
//     completeStay: {
//       type: Boolean,
//       default: false,
//     },
//     month: {
//       type: Boolean,
//       default: false,
//     },
//     inmediateReservation: {
//       type: Boolean,
//       default: false,
//     },
//     freeCancelation: {
//       type: Boolean,
//       default: Boolean,
//     },
//     flexibleIO: {
//       type: Boolean,
//       default: false,
//     },
//     surfaceShape: String,
//     country: {
//       type: String,
//       required: true,
//     },
//     address: {
//       type: String,
//       required: true,
//     },
//     street1: {
//       type: String,
//     },
//     street2: {
//       type: String,
//     },
//     city: {
//       type: String,
//       required: true,
//     },
//     province: {
//       type: String,
//       required: true,
//     },
//     postalCode: String,
//     parkingType: {
//       type: String,
//       required: true,
//       enum: ['commercial', 'particular'],
//       default: 'particular',
//     },
//     lots: [{ type: Schema.Types.ObjectId, ref: 'Lots' }],
//     services: {
//       security: [],
//       extras: [],
//     },
//     area: String,
//     allowedVehicles: {
//       bicycle: {
//         isAllowed: false,
//         list: [],
//         quantity: 0,
//         prices: {
//           hour: '',
//           halfStay: '',
//           completeStay: '',
//           month: '',
//         },
//       },
//       electricSkateboard: {
//         isAllowed: {
//           type: Boolean,
//           default: false,
//         },
//         list: [],
//         quantity: Number,
//         prices: {
//           hour: String,
//           halfStay: String,
//           completeStay: String,
//           month: String,
//         },
//       },
//       motorcycle: {
//         isAllowed: {
//           type: Boolean,
//           default: false,
//         },
//         list: [],
//         quantity: Number,
//         prices: {
//           hour: String,
//           halfStay: String,
//           completeStay: String,
//           month: String,
//         },
//       },
//       car: {
//         isAllowed: {
//           type: Boolean,
//           default: false,
//         },
//         list: [],
//         quantity: Number,
//         prices: {
//           hour: String,
//           halfStay: String,
//           completeStay: String,
//           month: String,
//         },
//       },
//       pickupTruck: {
//         isAllowed: {
//           type: Boolean,
//           default: false,
//         },
//         list: [],
//         quantity: Number,
//         prices: {
//           hour: String,
//           halfStay: String,
//           completeStay: String,
//           month: String,
//         },
//       },
//       truck: {
//         isAllowed: {
//           type: Boolean,
//           default: false,
//         },
//         list: [],
//         quantity: Number,
//         prices: {
//           hour: String,
//           halfStay: String,
//           completeStay: String,
//           month: String,
//         },
//       },
//       trailer: {
//         isAllowed: {
//           type: Boolean,
//           default: false,
//         },
//         list: [],
//         quantity: Number,
//         prices: {
//           hour: String,
//           halfStay: String,
//           completeStay: String,
//           month: String,
//         },
//       },
//       motorhome: {
//         isAllowed: {
//           type: Boolean,
//           default: false,
//         },
//         list: [],
//         quantity: Number,
//         prices: {
//           hour: String,
//           halfStay: String,
//           completeStay: String,
//           month: String,
//         },
//       },
//     },
//     photos: [],
//     isVerified: {
//       type: Boolean,
//       default: false,
//     },
//     isHidden: {
//       type: Boolean,
//       default: true, // hidden hasta que este verificado?
//     },
//     isDeleted: {
//       type: Boolean,
//       default: false,
//     },
//     locationGPS: {
//       type: { type: String, enum: ['Point'], default: 'Point' },
//       coordinates: [Number],
//     },
//   },
//   {
//     timestamps: true,
//   },
// )

const Progress = { type: String, enum: ['initial', 'in-progress', 'completed'], default: 'initial' }

const parkingSchema = new Schema(
  {
    createdBy: Schema.Types.ObjectId,
    status: {
      type: String,
      enum: ['in-progress', 'in-revision', 'unavailable', 'available', 'deleted'],
      default: 'in-progress',
    },
    parkingType: {
      progress: Progress,
      types: { type: String, enum: ['particular', 'commercial'] },
    },
    features: {
      progress: Progress,
      entry: {
        width: String,
        high: String,
        types: String,
        ramp: String,
      },
      roofing: {
        types: String,
        high: String,
      },
      accessibility: [String],
    },
    vehiclesAllowed: {
      progress: Progress,
      particular: {
        availableArea: String,
        shape: String,
        obstacles: Boolean,
        typesVehicles: {
          bici: Boolean,
          electricSkateboard: Boolean,
          motorcycle: [String],
          car: [String],
          pickupTruck: [String],
          trailer: [String],
          nautic: [String],
          houseTrailer: [String],
          truck: [String],
          other: {
            name: String,
            width: String,
            large: String,
            high: String,
          },
        },
      },
      commercial: {
        availableArea: String,
        shape: String,
        obstacles: Boolean,
        typesVehicles: {
          bici: {
            amount: { type: Number, default: 0 },
          },
          electricSkateboard: {
            amount: { type: Number, default: 0 },
          },
          motorcycle: {
            amount: { type: Number, default: 0 },
            types: [String],
          },
          car: {
            amount: { type: Number, default: 0 },
            types: [String],
          },
          pickupTruck: {
            amount: { type: Number, default: 0 },
            types: [String],
          },
          trailer: {
            amount: { type: Number, default: 0 },
            types: [String],
          },
          nautic: {
            amount: { type: Number, default: 0 },
            types: [String],
          },
          houseTrailer: {
            amount: { type: Number, default: 0 },
            types: [String],
          },
          truck: {
            amount: { type: Number, default: 0 },
            types: [String],
          },
          other: {
            amount: { type: Number, default: 0 },
            name: String,
            width: String,
            large: String,
            high: String,
          },
        },
      },
    },
    location: {
      progress: Progress,
      country: String,
      address: String,
      address1: String,
      address2: String,
      city: String,
      province: String,
      postalCode: String,
      type: { type: String, enum: ['Point'], default: 'Point' },
      coordinates: {
        lat: String,
        long: String,
      },
    },
    services: {
      progress: Progress,
      security: [String],
      extras: [String],
      additional: String,
    },
    photos: {
      progress: Progress,
      data: {},
    },
    info: {
      progress: Progress,
      name: { type: String },
      description: String,
      accessInstruction: String,
      termsAndConditions: String,
    },
    availability: {
      progress: Progress,
      time: {
        byHr: Boolean,
        prevAnnouncement: String,
        byHalfStay: Boolean,
        byStay: Boolean,
        byMonth: Boolean,
        intervalTime: String,
      },
      stay: {
        fullTime: Boolean,
        partTime: false,
        partTimeData: {
          monday: [],
          tuesday: [],
          wendesday: [],
          thursday: [],
          friday: [],
          saturday: [],
          sunday: [],
        },
      },
      additionals: {
        isImmediateReservation: Boolean,
        isCancelFree: Boolean,
        isFlexible: Boolean,
      },
    },
    calendar: {
      progress: Progress,
      unavailable: [String], // dias o mes no disponibles
    },
    prices: {
      progress: Progress,
      vehiclesPrice: {},
    },
    // discount: {
    //   progress: Progress,
    //   percentage: String,
    // },
  },
  { timestamps: true },
)

parkingSchema.index({ location: '2dsphere' })

export default mongoose.model('Parkings', parkingSchema)
