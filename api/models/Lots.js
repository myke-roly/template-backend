import mongoose from 'mongoose'

const { Schema } = mongoose

const lotSchema = new Schema({
  parkingId: {
    type: Schema.Types.ObjectId,
    ref: 'Parkings',
  },
  image: {
    type: String,
    required: false, // Luego cambiar a requerido
  },
  lotRef: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  features: Schema.Types.Mixed,
  isAvailable: {
    type: Boolean,
    default: true,
  },
  isHidden: {
    type: Boolean,
    default: false,
  },
  prices: Schema.Types.Mixed,
  rating: {
    type: Number,
    min: 1,
    max: 10,
  },
  dimensions: {
    width: Number,
    height: Number,
    length: Number,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
})

export default mongoose.model('Lots', lotSchema)
