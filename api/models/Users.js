import mongoose, { Schema } from 'mongoose'

import { hashUtils } from '../utils'

const adminId = mongoose.Types.ObjectId()
const customerId = mongoose.Types.ObjectId()

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    lastName: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
    },
    password: {
      type: String,
      required: true,
      minlength: 8,
      maxlength: 1024,
    },
    image: String,
    customerId: {
      type: Schema.Types.ObjectId,
      unique: true,
      default: adminId,
    },
    adminId: {
      type: Schema.Types.ObjectId,
      unique: true,
      default: customerId,
    },
    cellPhone: {
      type: String,
      required: true,
    },
    birthday: {
      type: String,
      required: false,
    },
    creditDebitCard: Number,
    favorites: [{ type: Schema.Types.ObjectId, ref: 'Lots' }],
    documents: {
      type: Schema.Types.Mixed,
      default: [],
    },
    validateEmail: {
      type: Boolean,
      default: false,
    },
    vehicles: [{ type: Schema.Types.ObjectId, ref: 'Vehicles' }],
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  },
)

// userSchema.methods.toJSON = function () {
//   const obj = this.toObject()
//   delete obj.password
//   return obj
// }

userSchema.pre('save', async function (next) {
  this.password = await hashUtils.hashPassword(this.password)
  next()
})

userSchema.methods.isValidPassword = async function (password) {
  const user = this
  const compare = await hashUtils.comparePassword(password, user.password)
  return compare
}

export default mongoose.model('Users', userSchema)
