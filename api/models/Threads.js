import mongoose from 'mongoose'

const { Schema } = mongoose

const threadsSchema = new Schema(
  {
    ownerId: {
      type: Schema.Types.ObjectId,
      ref: 'Users',
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'Users',
    },
  },
  {
    timestamps: true,
  },
)

export default mongoose.model('Threads', threadsSchema)
