import mongoose from 'mongoose'

const { Schema } = mongoose

const vehicleSchema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'Users',
      required: true,
    },
    brand: {
      type: String,
      required: true,
    },
    model: {
      type: String,
      required: true,
    },
    licensePlate: {
      type: String,
      required: true,
    },
    color: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
      // enum: ['car', 'motorcicle', 'suv', 'quad'], // A revisar
    },
    dimensions: {
      width: Number,
      height: Number,
      length: Number,
    },
  },
  {
    timestamps: true,
  },
)

export default mongoose.model('Vehicles', vehicleSchema)
