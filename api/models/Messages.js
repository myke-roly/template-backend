import mongoose from 'mongoose'

const { Schema } = mongoose

const messagesSchema = new Schema(
  {
    ownerId: {
      type: Schema.Types.ObjectId,
      ref: 'Users',
      required: true,
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'Users',
      required: true,
    },
    message: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  },
)

export default mongoose.model('Messages', messagesSchema)
