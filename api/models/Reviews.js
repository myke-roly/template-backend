import mongoose from 'mongoose'

const { Schema } = mongoose

const reviewSchema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'Users',
    },
    lotId: {
      type: Schema.Types.ObjectId,
      ref: 'Parkings',
    },
    comment: {
      type: String,
      required: true,
    },
    score: {
      type: Number,
      required: true,
    },
    isChecked: {
      type: Boolean,
      required: true,
      default: false,
    },
  },
  {
    timestamps: true,
  },
)

export default mongoose.model('Reviews', reviewSchema)
