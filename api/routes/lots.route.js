import express from 'express'

import {
  validateLot,
  createLot,
  getLots,
  updateLot,
  deleteLot,
  findLotsByParkingId,
} from '../controllers/lotsController'

const lotRouter = express.Router()

lotRouter.get('/', getLots)
lotRouter.get('/:id', findLotsByParkingId)
lotRouter.post('/', validateLot, createLot)
lotRouter.put('/:id', validateLot, updateLot)
lotRouter.delete('/:id', deleteLot)

export { lotRouter }
