import express from 'express'

// CONTROLLERS
import {
  createVehicle,
  findUserVehicles,
  deleteVehicle,
  updateVehicle,
} from '../controllers/vehicleController'

const vehicleRouter = express.Router()

vehicleRouter.get('/', findUserVehicles)
vehicleRouter.post('/', createVehicle)
vehicleRouter.put('/:id', updateVehicle)
vehicleRouter.delete('/:id', deleteVehicle)

export { vehicleRouter }
