import express from 'express'

import { authenticate } from 'api/features/authenticate'

import { findParkingsByUserId } from '../controllers/usersController'
import {
  createParking,
  getParkings,
  updateParking,
  deleteParking,
  getParking,
  getClosestParking,
  deleteParkings,
} from '../controllers/parkingController'

const parkingRouter = express.Router()

parkingRouter.post('/create', authenticate, createParking)
parkingRouter.get('/', getParkings)
parkingRouter.get('/closestParking', getClosestParking)
parkingRouter.get('/:id', authenticate, getParking)
parkingRouter.get('/myparkings/:id', authenticate, findParkingsByUserId)
parkingRouter.patch('/:id', authenticate, updateParking)
parkingRouter.delete('/:id', authenticate, deleteParking)
parkingRouter.delete('/all/:id', authenticate, deleteParkings)

export { parkingRouter }
