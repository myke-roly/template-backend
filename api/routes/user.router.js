import express from 'express'

// CONTROLLERS
import {
  // findUsers,
  findUserById,
  updateUserById,
  deleteUserById,
  findParkingsByUserId,
  validateUpdateUsers,
} from '../controllers/usersController'

const userRouter = express.Router()

// userRouter.get('/', findUsers)
userRouter.get('/:id', findUserById)
userRouter.get('/myparkings/:id', findParkingsByUserId)
userRouter.put('/:id', validateUpdateUsers, updateUserById)
userRouter.delete('/:id', deleteUserById)

export { userRouter }
