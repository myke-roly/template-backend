import express from 'express'

import { authenticate } from 'api/features/authenticate'

import { userRouter } from './user.router'
import { salesFeeRouter } from './salesFee.route'
import { reviewRouter } from './reviews.route'
import { messagesRouter } from './messages.route'
import { parkingRouter } from './parkings.route'
import { lotRouter } from './lots.route'
import { vehicleRouter } from './vehicles.route'
import { authRouter } from './auth.route'
import { bookingRouter } from './bookings.route'

const mainRouter = express.Router()

mainRouter.use('/api/auth', authRouter)
mainRouter.use('/api/users', authenticate, userRouter)
mainRouter.use('/api/parkings', parkingRouter)
mainRouter.use('/api/lots', authenticate, lotRouter)
mainRouter.use('/api/salesfee', salesFeeRouter)
mainRouter.use('/api/vehicles', authenticate, vehicleRouter)
mainRouter.use('/api/reviews', authenticate, reviewRouter)
mainRouter.use('/api/messages', authenticate, messagesRouter)
mainRouter.use('/api/bookings', bookingRouter)

export { mainRouter }
