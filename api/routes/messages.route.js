import express from 'express'

// CONTROLLERS
import {
  createMessage,
  getMessages,
  validateMessage,
  // deleteMessage,
  // updateMessage,
} from '../controllers/messagesController'

const messagesRouter = express.Router()

messagesRouter.get('/', getMessages)
messagesRouter.post('/', validateMessage, createMessage)
// reviewRouter.put('/:id', updateReview)
// reviewRouter.delete('/:id', deleteReview)

export { messagesRouter }
