import express from 'express'

// CONTROLLERS
import {
  createReview,
  validateReview,
  getReviews,
  getLotReviews,
  deleteReview,
  updateReview,
} from '../controllers/reviewsController'

const reviewRouter = express.Router()

reviewRouter.get('/', getReviews)
reviewRouter.get('/:lotId', getLotReviews)
reviewRouter.post('/:lotId', validateReview, createReview)
reviewRouter.put('/:id', updateReview)
reviewRouter.delete('/:id', deleteReview)

export { reviewRouter }
