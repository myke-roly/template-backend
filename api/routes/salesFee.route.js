import express from 'express'

import {
  createSalesFee,
  deleteSalesFee,
  getSalesFee,
  updateSalesFee,
  validateSalesFee,
} from '../controllers/salesFeeController'

const salesFeeRouter = express.Router()

salesFeeRouter.get('/', getSalesFee)
salesFeeRouter.post('/', validateSalesFee, createSalesFee)
salesFeeRouter.put('/:id', validateSalesFee, updateSalesFee)
salesFeeRouter.delete('/:id', deleteSalesFee)

export { salesFeeRouter }
