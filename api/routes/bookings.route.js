import express from 'express'

import {
  createBooking,
  getBooking,
  updateBooking,
  validateBooking,
} from '../controllers/bookingController'

const bookingRouter = express.Router()

bookingRouter.get('/', getBooking)
bookingRouter.post('/', validateBooking, createBooking)
bookingRouter.put('/:id', validateBooking, updateBooking)

export { bookingRouter }
