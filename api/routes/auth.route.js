import express from 'express'

import {
  loginUser,
  createUser,
  validateUsers,
  loginUserBySocialMedia,
} from '../controllers/usersController'

const authRouter = express.Router()

authRouter.post('/sign-up', validateUsers, createUser)

authRouter.post('/login', loginUser)

authRouter.post('/login-social-media', loginUserBySocialMedia)

export { authRouter }
