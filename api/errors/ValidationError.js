import { GenericError } from './GenericError'

const getJoiKeys = celebrate => {
  if (!celebrate.joi.details) return []

  return celebrate.joi.details.map(detail => detail.path.join('.'))
}

class ValidationError extends GenericError {
  constructor(error) {
    super(406, error.joi.message, 'ValidationError')
    this.validation = {
      source: error.meta.source,
      keys: getJoiKeys(error),
    }
  }
}

export { ValidationError }
