module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: ['airbnb-base', 'plugin:import/errors', 'plugin:prettier/recommended'],
  settings: { 'import/resolver': { 'babel-module': {} } },
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'prettier/prettier': 'error',
    'import/order': [
      'error',
      {
        groups: [['builtin', 'external'], 'internal', ['sibling', 'parent', 'index']],
        'newlines-between': 'always',
      },
    ],
    'import/extensions': 0,
    'no-param-reassign': [
      'error',
      {
        props: true,
      },
    ],
    'no-return-await': 'error',
    eqeqeq: 'error',
    'no-unneeded-ternary': 'error',
    'no-console': 'error',
    'import/prefer-default-export': 0,
    'no-underscore-dangle': 0,
    'consistent-return': 0,
    'func-names': 0,
    'no-extra-boolean-cast': 0,
  },
}
